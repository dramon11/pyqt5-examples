#!/usr/bin/python3
import os
import sys
from PyQt5 import QtWidgets, QtCore

try:
    from .backend import cli
except ImportError:
    from backend import cli


class Main(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

    @QtCore.pyqtSlot(dict)
    def cli(self, cmd):
        print(cmd)


def main(cmd):
    app = QtWidgets.QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(False)
    cmd = cli.parse(cmd)
    widget = Main(app)
    widget.cli(cmd)
    widgetBus = cli.QDBusObject(parent=widget)
    sys.exit(app.exec_())
