#!/usr/bin/python3
import sys
import time
from PyQt5 import QtWidgets, QtCore, QtSerialPort


class Serial(QtSerialPort.QSerialPort):
    def __init__(self):
        super().__init__()
        self.readyRead.connect(self.read)

    def ports(self):
        available = []
        for port in QtSerialPort.QSerialPortInfo.availablePorts():
            available.append(port.systemLocation())
        if available:
            available = str(available)
            return(available.translate({ord(c): None for c in "[']"}))
        return("none")

    def connect(self, port, baudrate):
        self.setPortName(port)
        self.setBaudRate(baudrate)
        if not self.open(QtCore.QIODevice.ReadWrite):
            print("Could not connect to device on port %s" % port)

    def read(self):
        data = self.readLine()
        data = bytes(data).decode("utf8")
        print(data, end="")

    def send(self, data):
        data = data.encode()
        self.write(data)

    def reset(self):
        self.setDataTerminalReady(False)
        time.sleep(0.022)
        self.setDataTerminalReady(True)


class Main(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.serial = Serial()
        print("Currently available ports: %s" % self.serial.ports())
        self.serial.connect("/dev/ttyUSB0", 9600)


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    gui = Main()
    sys.exit(app.exec_())
