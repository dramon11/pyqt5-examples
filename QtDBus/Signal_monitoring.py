#!/usr/bin/python3
import sys
from PyQt5 import QtCore, QtWidgets, QtDBus


class Main(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        service = "org.freedesktop.login1"
        if QtDBus.QDBusConnection.systemBus().interface().isServiceRegistered(service).value():
            path = "/org/freedesktop/login1"
            interface = "org.freedesktop.login1.Manager"
            name = "PrepareForShutdown"
            self.bus = QtDBus.QDBusConnection.systemBus()
            self.bus.connect(service, path, interface, name, self._slot)
            self.bus.connect(service, path, interface, name, "b", self._slotOverloaded)

    @QtCore.pyqtSlot()
    def _slot(self):
        print("Got 'PrepareForSleep' Signal")

    @QtCore.pyqtSlot(bool)
    def _slotOverloaded(self, data):
        print(f"Got '{data}' from 'PrepareForSleep' Signal")


if __name__== '__main__':
    app = QtWidgets.QApplication(sys.argv)
    daemon = Main(app)
    sys.exit(app.exec_())
