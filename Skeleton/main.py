#!/usr/bin/python3
import os
import sys
from PyQt5 import QtWidgets, QtCore, uic

try:
    from .ui import skeleton_gui
    from .__id__ import ID, FULLNAME
    from .backend import cli
    from .backend import database
except ImportError:
    from __id__ import ID, FULLNAME
    from backend import cli
    from backend import database

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))
PREFERENCES_FILE = os.path.expanduser(f"~/.config/{ID}/preferences.json")


class Main(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.preferences = database.Database(PREFERENCES_FILE)
        self._uiInit()
        self.ui.show()

    def _cancel(self):
        self.parent.quit()

    def _ok(self):
        self.parent.quit()

    def _uiInit(self):
        try:
            self.ui = skeleton_gui.Ui_Dialog()
            self.ui.setupUi(self)
        except NameError:
            self.ui = uic.loadUi(f"{LOCAL_DIR}/ui/skeleton_gui.ui", self)
        self.ui.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).clicked.connect(self._ok)
        self.ui.buttonBox.button(QtWidgets.QDialogButtonBox.Cancel).clicked.connect(self._cancel)

    @QtCore.pyqtSlot(dict)
    def cli(self, cmd):
        if "quit" in cmd:
            self.parent.quit()


def main(cmd=""):
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName(FULLNAME)
    app.setDesktopFileName(ID)
    app.setQuitOnLastWindowClosed(False)
    cmd = cli.parse(cmd)
    widget = Main(app)
    widget.cli(cmd)
    widgetBus = cli.QDBusObject(parent=widget)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
