#!/usr/bin/python3
import os
import sys
from PyQt5 import QtGui, QtWidgets, QtCore

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))


class Main(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.icons = {}
        for icon in os.listdir(LOCAL_DIR):
            basename, ext = os.path.splitext(icon)
            if ext == ".svg":
                self.icons[basename] = QtGui.QIcon(f"{LOCAL_DIR}/{icon}")

        # SVG to pixmap conversion (KDE compatibility)
        trayIcon = QtGui.QIcon(self.icons["tray"].pixmap(64, 64))

        self.trayIcon = QtWidgets.QSystemTrayIcon()
        self.trayIcon.setIcon(trayIcon)
        self.trayIcon.show()

        # Init tooltip timers
        self.showTooltipTimer = QtCore.QTimer(singleShot=True)
        self.showTooltipTimer.timeout.connect(self.tooltipDisplay)
        self.hideTooltipTimer = QtCore.QTimer(singleShot=True)
        self.hideTooltipTimer.timeout.connect(QtWidgets.QToolTip.hideText)

        # Call tooltip example
        self.tooltipCall("Hello world")

    def tooltipCall(self, tooltip):
        self.trayIcon.setToolTip(tooltip)
        self.showTooltipTimer.start(200)
        self.hideTooltipTimer.start(3000)

    def tooltipDisplay(self):
        pos = QtCore.QRect(self.trayIcon.geometry())
        pos = QtCore.QPoint(pos.x(), pos.y())
        QtWidgets.QToolTip.showText(pos, self.trayIcon.toolTip(), self)


def main():
    app = QtWidgets.QApplication(sys.argv)
    gui = Main(app)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
