#!/usr/bin/python3
import os
import sys
from PyQt5 import QtGui, QtWidgets, QtCore

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__))


class Main(QtWidgets.QMainWindow):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.icons = {}
        for icon in os.listdir(LOCAL_DIR):
            basename, ext = os.path.splitext(icon)
            if ext == ".svg":
                self.icons[basename] = QtGui.QIcon(f"{LOCAL_DIR}/{icon}")

        # SVG to pixmap conversion (KDE compatibility)
        trayIcon = QtGui.QIcon(self.icons["tray"].pixmap(64, 64))

        self.trayIcon = QtWidgets.QSystemTrayIcon()
        self.trayIcon.activated.connect(self._clickEvent)
        self.trayIcon.setIcon(trayIcon)
        self.trayIcon.show()
        self.show()

    def changeEvent(self, event):
        # Override minimize event
        if event.type() == QtCore.QEvent.WindowStateChange:
            if self.windowState() & QtCore.Qt.WindowMinimized:
                self.setWindowState(QtCore.Qt.WindowNoState)
                self.hide() if self.isVisible() else self.show()

    def _clickEvent(self, event):
        if event == QtWidgets.QSystemTrayIcon.Trigger:
            self.show()


def main():
    app = QtWidgets.QApplication(sys.argv)
    gui = Main(app)
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
